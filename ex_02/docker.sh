#!/bin/bash

docker run -d --name nginx1 -v sharedvolume:/usr/share/nginx/html -p 8080:80 nginx:latest
docker run -d --name nginx2 -v sharedvolume:/usr/share/nginx/html -p 9090:80 nginx:latest

docker run -ti --name debian -v sharedvolume:/usr/share/exercicio2 debian-nano

touch /usr/share/exercicio2/hello.txt
echo Hello World > /usr/share/exercicio2/hello.txt

exit
